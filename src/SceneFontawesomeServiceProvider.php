<?php

namespace DecideNow\SceneFontawesome;

use Illuminate\Support\ServiceProvider;

class SceneFontawesomeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
		//
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
		$this->loadViewsFrom(__DIR__.'/Views/scene', 'scene.fontawesome');
		$this->mergeConfigFrom(__DIR__.'/Config/scene.php', 'scene.extensions');
		$this->publishes([
			__DIR__.'/Assets' => public_path(),
		], 'scene-fontawesome-assets');
    }
}
