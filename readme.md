# Scene Fontawesome #

## Описание ##
Расширение для библиотеки Scene для Laravel, позволяющее использовать Fontawesome 5.

## Установка ##
Исходный код проекта "живёт" на Bitbucket, поэтому для начала необходимо подключить репозиторий, а затем добавить пакет:

```
composer config repositories.decidenow.scene.fontawesome vcs https://bitbucket.org/decidenowlib/scene-fontawesome
composer require decidenow/scene-fontawesome
```

Файлы Fontawesome должны размещаться в общедоступном каталоге, поэтому их необходимо опубликовать:
```
php artisan vendor:publish --tag=scene-fontawesome-assets
```

## Начало работы ##
Можно использовать иконочные шрифты Fontawesome 5 в создаваемых сценах.

## Автор ##
Соколов Александр

E-mail: sanya_sokolov@inbox.ru
